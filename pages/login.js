import React, { Component } from 'react'
import styles from '../styles/Login.module.css'
import LoginComponent from '../components/Login'

export default function Login() {
    return (
        <div className={styles.container}>
            <LoginComponent />
        </div>
    )
}