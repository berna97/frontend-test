import '../styles/globals.css'
import NavbarComponent from '../components/Navbar'
import {AuthProvider} from '../contexts/AuthContext'

function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      <div className='Wrapper'>
        <NavbarComponent />
        <Component {...pageProps} />
      </div>
    </AuthProvider>
  )
}

export default MyApp
