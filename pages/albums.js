import React, { useEffect } from 'react'
import { useAuth } from '../contexts/AuthContext'
import Users from '../components/Users'

export default function Albums() {
    const {isUserLogged} = useAuth()

    useEffect(() => {
        isUserLogged();
    }, [])

    return (
        <div className="container">
            <Users type='albums'/>
        </div>
    )
}
