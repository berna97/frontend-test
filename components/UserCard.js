import React from 'react'
import styles from '../styles/UserCard.module.css'
import Image from 'next/image'

export default function UserCardComponent({cardData,toggle}) {
    return (
            <div onClick={() => toggle(cardData)} className={styles.userCard} key={cardData.id}>
                <div className={styles.card}>
                    {cardData.avatar  &&
                        <Image  width={100} height={100} key={cardData.avatar} src={cardData.avatar} />
                    }
                </div>
                <p>{cardData.first_name}</p>
                <span>{cardData.email}</span>
            </div>
    )
}
