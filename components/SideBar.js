import axios from 'axios';
import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import styles from '../styles/SideBar.module.css'

export default function SideBarComponent({user,isSideBarOpen, type}) {
    const [posts, setPosts] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        if(type === 'posts') {
            const postsURL = `https://jsonplaceholder.typicode.com/posts?userId=${user.id}`
            axios.get(postsURL)
            .then(response =>{
                setPosts(response.data)
                setLoading(false)
            })
            .catch(error => console.log(error))
        } else if(type === 'albums') {
            const albumURL = `https://jsonplaceholder.typicode.com/users/${user.id}/albums`
            axios.get(albumURL)
            .then(response =>{
                setPosts(response.data)
                setLoading(false)
            })
            .catch(error => console.log(error))
        }
    }, [user,user.id])


    return (
        <div className={'sideBarContainer ' + (isSideBarOpen ? 'open' : 'close')}>
            <div className='sideBarContainer-img'>
                {user.avatar && 
                    <Image width={100} height={100} src={user.avatar} />
                }
            </div>
            <p className='sideBarContainer-name'>{user.first_name}{user.last_name}</p>
            <p className='sideBarContainer-email'>{user.email}</p>

            {type === 'posts' && 
                <>
                    <h3 className='sideBarContainer-title'>Posts</h3>
                    <ul className='sideBarContainer-posts'>
                    {!!posts.length && !loading &&
                        posts.map(post => {
                            return (
                                    <li key={post.id}>{post.title}</li>
                                
                            )
                        })
                    }     
                </ul> 
                </>
            }
            {type === 'albums' &&
                <>
                    <h3 className='sideBarContainer-title'>Albums</h3>
                    <ul className='sideBarContainer-posts'>
                    {!!posts.length && !loading &&
                        posts.map(post => {
                            return (
                                <li key={post.id}>{post.title}</li>
                            )
                        })
                    }     
                </ul> 
                </>
            }
        </div>
    )
}
