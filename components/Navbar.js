import React, { Component, useEffect, useState } from 'react'
import { useAuth } from '../contexts/AuthContext'
import Home from '../public/assets/home.png'
import Album from '../public/assets/album.png'
import Logout from '../public/assets/logOut.png'
import styles from '../styles/NavBar.module.css'
import Image from 'next/image'
import Link from 'next/link'

export default function NavbarComponent() {
    const [userLogged, setUserLogged] = useState(false);
    const {value, logout} = useAuth();
    const menu = [
         {
            icon:Home,
            title:'Home',
            href: '/'
        },
         {
            icon:Album,
            title:'Albums',
            href: '/albums'
        },
    ]

    useEffect(() => {
        if(value.user.token) {
            setUserLogged(true)
        } else {
            setUserLogged(false)
        }
    }, [value])

    return (
        <>
            {
                userLogged && 
                <nav className={styles.navBar}>
                    <div className={styles.navBarLogo}>
                        <Image  width={100} height={50} src="https://yaydoo.com/wp-content/uploads/2021/07/yaydoo-white.png" alt="logo" />
                    </div>
                    <ul>
                    {
                        menu.map((item,index)=>{
                            return (
                                <Link href={item.href} key={index}>
                                    <li className={styles.navBarList}>
                                        { item.icon &&
                                            <Image width={20} height={20} src={item.icon}/>
                                        }
                                        <p>{item.title}</p>
                                    </li>
                                </Link>
                            )
                        })
                    }
                    </ul>
                    <div onClick={logout} className={styles.navBarLogOut}>
                        {Logout && 
                            <Image width={20} height={20} src={Logout} alt="logOut" />
                        }
                        <p>Log Out</p>
                    </div>
                </nav>
            }
        </>
    )
}