import React, { useEffect, useState, useCallback } from 'react'
import styles from '../styles/Home.module.css'
import { useAuth } from '../contexts/AuthContext'
import UserCardComponent from '../components/UserCard'
import axios from 'axios'
import SideBarComponent from '../components/SideBar'

export default function UsersComponent({type}) {
    const [users, setUsers] = useState([]);
    const [sideBar, setSideBar] = useState(false);
    const [selectedUser, setSelectedUser] = useState({})
    const {isUserLogged} = useAuth();

    useEffect(() => {
        isUserLogged();
        getUsers();
    }, [])

    const getUsers = () => {
      const usersURL = 'https://reqres.in/api/users'
      axios.get(usersURL)
        .then(response => {
          setUsers(response.data.data)
        })
        .catch(error => console.log(error))
    }

    const toggleSidebar = (user) => {
      if(sideBar) {
        setSideBar(false)
        setSelectedUser({})
      } else if(!sideBar) {
        setSideBar(true)
        setSelectedUser(user);
      };
    }

  return (
    <div className={styles.Users}>
      {sideBar &&
        <div onClick={()=>setSideBar(false)} className={styles.overlay}/>
      }
          {!!users.length &&
          users.map((user) => {
            return (
              <UserCardComponent cardData={user} key={user.id} toggle={toggleSidebar}/>
            );
          })}
      <SideBarComponent isSideBarOpen={sideBar} user={selectedUser} type={type}/>
    </div>
  )
}