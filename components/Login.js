import React, { useState, useEffect } from 'react'
import styles from '../styles/Login.module.css'
import {useAuth} from '../contexts/AuthContext'
import Image from 'next/image'

export default function LoginComponent() {
    const {login, isUserLoggedLogin} = useAuth();
    const [data, setData] = useState({
        email:'',
        password:''
    })

    const handleInputChange = (e) => {
        setData({...data, [e.target.name]: e.target.value})
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        login(data);
    }

    useEffect(() => {
        isUserLoggedLogin();
    }, [])

    return (
        <>  
            <div className={styles.formContainer}>
                <div className={styles.formContainerLeft}>
                    <Image width={150} height={75} src="https://yaydoo.com/wp-content/uploads/2021/07/yaydoo-white.png" alt="logo" />
                </div>
                <div className={styles.formContainerRight}>
                    <h1>Registration</h1>
                    <form onSubmit={handleSubmit}>
                        <input
                        placeholder="Email*"
                        type="email"
                        name="email"
                        onChange={handleInputChange}
                        />
                        <input
                            placeholder="Password*"
                            type="password"
                            name="password"
                            onChange={handleInputChange}
                        />
                    <button type="submit">
                        Submit
                    </button>
                    <span className={styles.formContainerRightPass}>Forgot your password?</span>
                    <span className={styles.formContainerRightStart}>Don&apos;t have an account? <span>Get Started</span></span>
                    </form>
                </div>
            </div>
        </>
    
    )
}