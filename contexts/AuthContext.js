import { useRouter } from 'next/router';
import axios from 'axios'
import React, { useState, useContext } from 'react'

const AuthContext = React.createContext();

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({children}) {
    const router = useRouter();
    const [user, setUser] = useState({})
    const value = {
        user
    }

    function login(data) {
        const loginURL = 'https://reqres.in/api/login' 
        axios.post(loginURL,data)
            .then(response => {
                if(response.data.token) {
                    setUser({...data,token: response.data.token})
                    router.push('/');
                }
            })
            .catch(error => console.error(error))
    }

    function logout() {
        setUser({});
        router.push('/login');
    }

    function isUserLogged() {
        if(!value.user.token) {
            router.push('/login');
        }
    }

    function isUserLoggedLogin() {
        if(value.user.token) {
            router.push('/')
        }
    }

    return (
        <AuthContext.Provider value={{value, login, logout, isUserLogged, isUserLoggedLogin}}>
            {children}
        </AuthContext.Provider>
    )
}
